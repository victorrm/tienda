import sys

articulos = {}

def anadir(nombre, precio):
    articulos[nombre] = precio

def mostrar():
    print("Lista de artículos en la tienda:")
    for nombre, precio in articulos.items():
        print(f"{nombre}: {precio} euros")

def pedir_articulo():
    while True:
        articulo = input("Artículo: ")
        if articulo in articulos:
            return articulo
        else:
            print("El artículo no está en la lista. Por favor, elige uno de la lista.")

def pedir_cantidad():
    while True:
        cantidad_str = input("Cantidad: ")
        try:
            cantidad = float(cantidad_str)
            return cantidad
        except ValueError:
            print("La cantidad ingresada no es válida. Introduce un número válido.")

def main():
    if len(sys.argv) % 2 != 1:
        print("Error en argumentos: Debes proporcionar una lista de artículos y sus precios.")
        return

    for i in range(1, len(sys.argv), 2):
        nombre = sys.argv[i]
        precio_str = sys.argv[i + 1]
        try:
            precio = float(precio_str)
            anadir(nombre, precio)
        except ValueError:
            print(f"Error en argumentos: {precio_str} no es un precio correcto.")
            return

    mostrar()

    total = 0.0

    while True:
        articulo = pedir_articulo()
        cantidad = pedir_cantidad()
        precio_articulo = articulos[articulo]
        total += cantidad * precio_articulo

        print(f"Compra total: {cantidad} de {articulo}, a pagar {cantidad * precio_articulo} euros")

if __name__ == "__main__":
    main()
